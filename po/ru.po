# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the hibernate@dafne.rocks package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# SlyDeath <slyrs@yandex.ru>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: hibernate@dafne.rocks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-09-16 00:00+0800\n"
"PO-Revision-Date: 2021-07-18 11:00+0300\n"
"Last-Translator: SlyDeath <slyrs@yandex.ru>\n"
"Language-Team: Russian <slyrs@yandex.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)\n"
"X-Generator: Gtranslator 40.0\n"

#. Create hibernate action item
#: src/extension.js:60
msgid "Hibernate"
msgstr "Гибернация"
